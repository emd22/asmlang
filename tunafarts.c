#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>

#define IS_NUMBER 0x01
#define IS_STRING 0x02
#define IS_VARIABLE 0x04

#define OP_DECL 0
#define OP_GETV 1
#define OP_ASM  2
#define OP_JMPD 3
#define OP_JMPV 4

typedef struct {
    const char *keyw;
    const int wait_on;
} keyword_t;

static FILE *asm_out  = NULL;
static char *asm_data = NULL;
static int   asm_idx  = 0;
static int   asm_blen = 128*128;
static int   asm_out_len = 0;

int asm_get_loc(char *text) {
    int i;
    int text_idx = 0;
    for (i = 0; i < asm_idx; i++) {
        if (asm_data[i] == text[text_idx]) {
                  
        }
    }
}

void asm_cpy(char *data) {
    do {
        asm_data[asm_idx++] = (*data);
        if (asm_idx == asm_blen) {
            asm_data = realloc(asm_data, asm_blen*2);
            printf("reallocating buffer...\n");
        }
    } while (*(data++) != 0);
}

void do_op(int op, char ops[16][16]) {
    switch (op) {
        case OP_DECL:
            printf("%s:\n", ops[0]);
            break;
    }
}

void end_tok(char *tok, int tokidx) {
    static char next_tokens[16][16] = { '\0' };
    static int  next_tokens_idx = 0;
    static int  waiting_on = 0;
    static int  waiting_kw = 0;

    keyword_t keywords[16] = {
        {"decl", 1},
        {"getv", 1},
        {"asm",  1},
        {"jmpd", 1},
        {"jmpv", 1}
    };
    
    if (waiting_on > 0) {
        strcpy(next_tokens[next_tokens_idx++], tok);
        waiting_on--;
        
    }
    if (waiting_on == 0 && next_tokens_idx != 0) {
        do_op(waiting_kw, next_tokens);
        next_tokens_idx = 0;
        return;
    }

    const int keywords_len = 5;
    keyword_t *kw;

    int i;
    for (i = 0; i < keywords_len; i++) {
        kw = &keywords[i];
        if (!strcmp(tok, kw->keyw)) {
            if (waiting_on != 0) {
                printf("ERROR: keyword %s was called with invalid arguments.(requires %d)\n", kw->keyw, kw->wait_on);            
            }
            waiting_on = kw->wait_on;
            waiting_kw = i;
        }
    }
}

void parse(char *str) {
    char ch;
    char flags = 0;
    char tok[16];
    int tokidx = 0;    
    do {
        ch = (*str);
        if (ch == '#') {
            flags ^= IS_NUMBER;
            continue;
        }
        else if (ch == ' ' || ch == '\t' || ch == '\0') {
            end_tok(tok, tokidx);
            tokidx = 0;
            memset(tok, 0, 16);
            continue;
        }
        tok[tokidx++] = ch;
    } while (*(str++) != 0);
}

void sig_handler(int signum) {
    fwrite(asm_data, 1, asm_idx, asm_out);
    printf("caught sigint, exitting...\n");
    fclose(asm_out);
    exit(0);
}

int main(void) {
    asm_out = fopen("out.S", "wb");
    asm_data = malloc(128*128);

    signal(SIGINT, sig_handler);

    asm_cpy(".text\n\n_start:\n");

    char str[64];
    while (1) {
        scanf("%64s", str);
        parse(str);
    }
    return 0;
}
